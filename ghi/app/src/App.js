import {BrowserRouter, Routes, Route,} from 'react-router-dom';
import LocationForm from './LocationForm'
import AttendeesList from './AttendeesList';
import Nav from './Nav';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm'
import NewPresentationForm from './NewPresentationForm';
import MainPage from './MainPage';



function App(props) {  
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div>
        <Routes>
          <Route index element = {<MainPage />} />
          <Route path = '/locations/new' element={<LocationForm />} />
          <Route path='/attendees' element={<AttendeesList attendees = {props.attendees} />} />
          <Route path='/conferences/new' element= {<ConferenceForm />} />
          <Route path='/attendees/new' element={<AttendConferenceForm conference = {props.conferences}/>} />
          <Route path='/presentations/new' element={<NewPresentationForm conferece = {props.conferences} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
