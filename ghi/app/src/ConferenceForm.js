import React, {useEffect, useState} from 'react';

export default function ConferenceForm(){
    const [name, setName] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');
    const [description, setDescription]= useState('') ;
    const [maxPresentations, setMaxPresentations]= useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [locations, setLocations] = useState([]);
    const[locationChoice, setLocationChoice] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleStartDate = (event) => {
        const value = event.target.value;
        setStartDate(value);
    };

    const handleEndDate = (event) => {
        const value = event.target.value;
        setEndDate(value);
    };

    const handleDescription = (event) => {
        const value = event.target.value;
        setDescription(value);
    };

    const handleMaxPresentations = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    };

    const handleMaxAttendees = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    };

    const fetchData = async() =>{
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    };
    useEffect(()=>{
        fetchData();
    }, []);

    const handleLocationChoice = (event) => {
        const value = event.target.value;
        setLocationChoice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data ={};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description= description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = locationChoice;
        
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type' : 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocationChoice('');
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} 
                            value={name} 
                            placeholder="Name" 
                            required type="text" 
                            id="name" 
                            name="name" 
                            className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartDate} 
                            value={startDate} 
                            placeholder="Start Date" 
                            required type="date" 
                            id="start_date" 
                            name="starts" 
                            className="form-control" />
                            <label htmlFor="start_date">Start Date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndDate} 
                            value={endDate}
                            placeholder="End Date" 
                            required 
                            type="date" 
                            id="end_date" 
                            name="ends" 
                            className="form-control" />
                            <label htmlFor="end_date">End Date</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description" className="form-label">Description</label>
                            <textarea onChange={handleDescription} 
                            value={description} 
                             placeholder="Write a description of your conference." 
                             required 
                             rows="4" 
                             id="description" 
                             name="description" 
                             className="form-control"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentations} 
                            value = {maxPresentations}
                            placeholder="Max presentations" 
                            required 
                            type="number" 
                            id="max_presentations" 
                            name="max_presentations" 
                            className="form-control" />
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendees} 
                            value = {maxAttendees}
                            placeholder="Max attendees" 
                            required 
                            type="number" 
                            id="max_attendees" 
                            name="max_attendees" 
                            className="form-control" />
                            <label htmlFor="max_attendees">Max attendees</label>
                        </div>
                        <div className="mb-3"> 
                            <select onChange={handleLocationChoice}
                            value= {locationChoice}
                            required id="location" 
                            name="location" 
                            className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location =>{
                                return(
                                    <option key ={location.id} value = {location.id}>
                                        {location.name}
                                    </option>
                                );
                            })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        
    );
}