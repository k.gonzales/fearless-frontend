import React, {useEffect, useState} from "react";

export default function NewPresentationForm() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [companyName, setCompanyName] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conferences, setConferences] = useState([])
    const [conferenceChoice, setConferenceChoice] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    };

    const handleChangeEmail = (event) => {
        const value = event.target.value;
        setEmail(value);
    };

    const handleCompanyName = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    };

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    };

    const handleSynopsis = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    };
    
    const fetchData = async() => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
            console.log(data)
        }
    };

    useEffect(()=>{
        fetchData();
    }, []);

    const handleConferenceChoice = (event) => {
        const value = event.target.value;
        setConferenceChoice(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.presenter_name = name;
        data.company_name = companyName;
        data.presenter_email = email;
        data.title = title;
        data.synopsis = synopsis;

        const presentationUrl = `http://localhost:8000${conferenceChoice}presentations/`;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const presentationResponse = await fetch(presentationUrl, fetchOptions);
        if (presentationResponse.ok) {
            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConferenceChoice('');
        }
    }
    
    
    return(
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new presentation</h1>
                        <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange}
                            value={name}
                            placeholder="Presenter name" 
                            required type="text" 
                            name="presenter_name" 
                            id="presenter_name" 
                            className="form-control" />
                            <label htmlFor="presenter_name">Presenter name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleChangeEmail}
                            value={email}
                            placeholder="Presenter email" 
                            required type="email" 
                            name="presenter_email" 
                            id="presenter_email" 
                            className="form-control" />
                            <label htmlFor="presenter_email">Presenter email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCompanyName}
                            value={companyName}
                            placeholder="Company name" 
                            type="text" 
                            name="company_name" 
                            id="company_name" 
                            className="form-control" />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange}
                            value={title}
                            placeholder="Title" 
                            required type="text" 
                            name="title" 
                            id="title" 
                            className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="synopsis">Synopsis</label>
                            <textarea onChange={handleSynopsis}
                            value={synopsis}
                            className="form-control" 
                            id="synopsis" 
                            rows="3" 
                            name="synopsis">
                            </textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConferenceChoice} 
                            value = {conferenceChoice}
                            required name="conference" 
                            id="conference" 
                            className="form-select">
                                <option value="">Choose a conference</option>
                                {conferences.map(conference =>{
                                    return(
                                        <option key={conference.href} value = {conference.href} >
                                            {conference.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}