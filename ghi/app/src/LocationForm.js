import React, {useEffect, useState} from 'react';


export default function LocationForm(){
  

  //sets the useState hook to store the initial value of the name variable to an empty string
  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [roomCount, setRoomCount] = useState('');
  const handleRoomCount = (event) =>{
    const value = event.target.value;
    setRoomCount(value);
  }

  const [city, setCity] = useState('');
  const handleCityChange = (event) =>{
    const value = event.target.value;
    setCity(value);
  }


  //sets constant variable [varName, setVarName] = useStateFunc(initial state)
  const [states, setStates] = useState([]);
  const fetchData = async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setStates(data.states) //calls setStates and updates state to the returned list of states and abbrieviations
    } 
  };

  const [stateChoice, setStateChoice] = useState('')
  const handleStateChoice= (event) =>{
  const value = event.target.value;
  setStateChoice(value);
  }

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data ={};
    data.room_count = roomCount;
    data.name= name;
    data.city = city;
    data.state = stateChoice;

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      const newLocation = await response.json();
      console.log(newLocation);

      setName('');
      setRoomCount('');
      setCity('');
      setStateChoice('');
    }
  }

  return(
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
                <input onChange={handleNameChange} 
                value = {name}
                placeholder='Name' 
                required type='text' 
                name='name' 
                id='name' 
                className='form-control' />
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleRoomCount} 
                value = {roomCount}
                placeholder="Room count" 
                required type="number" 
                id="room_count" 
                name="room_count" 
                className="form-control"/>
                <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleCityChange} 
                value = {city}
                placeholder="City" 
                required type="text" 
                id="city" 
                name="city" 
                className="form-control"/>
                <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
                <select onChange={handleStateChoice} 
                value={stateChoice}
                required 
                id="state" 
                name="state" 
                className="form-select">
                <option  value="">Choose a state</option>
                {states.map(state=>{
                  return(
                    <option key = {state.abbrieviation} value={state.abbrieviation}>
                      {state.name}
                    </option>
                  );
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}