import {NavLink} from 'react-router-dom';

function Nav() {
    return (
        <>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to={"/"}>Conference GO!</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <NavLink to={'/'} >Home</NavLink>
                <NavLink to={'/locations/new'}>New location</NavLink>
                <NavLink to={`/conferences/new`}>New Conference</NavLink>
                <NavLink to={'/attendees/new'}>Attend Conference</NavLink>
            </div>
        </nav>
        </>
    );
}
export default Nav